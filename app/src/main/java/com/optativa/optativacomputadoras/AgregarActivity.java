package com.optativa.optativacomputadoras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.optativa.optativacomputadoras.BaseDeDatosSqlite.OpenHelper;

public class AgregarActivity extends AppCompatActivity {

    EditText txtNserie, txtDescripcion, txtProcesador, txtConectividad, txtMemoria, txtDiscoduro, txtSistemaop, txtPuertos;
    Button btnAgregar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar);
        txtNserie = findViewById(R.id.txtNserie);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtProcesador = findViewById(R.id.txtProcesador);
        txtConectividad = findViewById(R.id.txtConectividad);
        txtMemoria = findViewById(R.id.txtMemoria);
        txtDiscoduro = findViewById(R.id.txtDiscoduro);
        txtSistemaop = findViewById(R.id.txtSistemaop);
        txtPuertos = findViewById(R.id.txtPuertos);
        btnAgregar = (Button) findViewById(R.id.btnAgregar);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Agregar();
            }
        });


    }

    private void Agregar() {

        //Establecer conexion con la clase OpenHelper para acceder a la base de datos
        OpenHelper conexion = new OpenHelper(this, "Computador", null, 1);
        //Pone a la base de datos en modo de escritura
        SQLiteDatabase db = conexion.getWritableDatabase();

        //Asigna variables de tipo String a los campos de textos
        String nserie = txtNserie.getText().toString();
        String descripcion = txtDescripcion.getText().toString();
        String procesador = txtProcesador.getText().toString();
        String conectividad = txtConectividad.getText().toString();
        String memoria = txtMemoria.getText().toString();
        String discoduro = txtDiscoduro.getText().toString();
        String sistemaop = txtSistemaop.getText().toString();
        String puertos = txtPuertos.getText().toString();


        //Validacion de campos
        if (!nserie.isEmpty() && !descripcion.isEmpty()
                && !procesador.isEmpty() && !conectividad.isEmpty() && !memoria.isEmpty()
                && !discoduro.isEmpty() && !sistemaop.isEmpty() && !puertos.isEmpty() ){

            //Clase ContentValues para guardar los valores
            ContentValues valores = new ContentValues();
            //Metodo put para agregar al objeto
            valores.put("nserie", nserie);
            valores.put("descripcion", descripcion);
            valores.put("procesador", procesador);
            valores.put("conectividad", conectividad);
            valores.put("memoria", memoria);
            valores.put("discoduro", discoduro);
            valores.put("sistemaop", sistemaop);
            valores.put("puertos", puertos);

            //Inserta a la tabla Computador los datos que se guardan en el objeto ContentValues

            db.insert("Computador", null, valores);
            Toast.makeText(this, "Se agregó con exito", Toast.LENGTH_SHORT).show();
            //Se cierra la base de datos
            db.close();
            txtNserie.setText("");
            txtDescripcion.setText("");
            txtProcesador.setText("");
            txtConectividad.setText("");
            txtMemoria.setText("");
            txtDiscoduro.setText("");
            txtSistemaop.setText("");
            txtPuertos.setText("");

        } else {
            Toast.makeText(this, "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show();
        }

    }

}