package com.optativa.optativacomputadoras;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.optativa.optativacomputadoras.BaseDeDatosSqlite.OpenHelper;

public class BuscarActivity extends AppCompatActivity {

    EditText txtBuscarNserie;
    Button btnBuscar;
    TextView lblNserie, lblDescripcion, lblprocesador, lblConectividad, lblMemoria, lblDiscoduro, lblSitemaop, lblPuertos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar);

        txtBuscarNserie = findViewById(R.id.txtBuscarNserie);
        lblNserie = findViewById(R.id.lblNserie);
        lblDescripcion = findViewById(R.id.lblDescripcion);
        lblprocesador = findViewById(R.id.lblProcesador);
        lblConectividad = findViewById(R.id.lblConectividad );
        lblMemoria = findViewById(R.id.lblMemoria);
        lblDiscoduro = findViewById(R.id.lblDiscoduro);
        lblSitemaop = findViewById(R.id.lblSistemaop);
        lblPuertos = findViewById(R.id.lblPuertos);

        btnBuscar = (Button) findViewById(R.id.btnBusqueda);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String id = txtBuscarNserie.getText().toString();
                    OpenHelper conexion = new OpenHelper(getApplicationContext(), "Computador", null, 1);
                    SQLiteDatabase db = conexion.getReadableDatabase();
                    Cursor fila = db.rawQuery("select nserie, descripcion, procesador, conectividad, memoria, discoduro, sistemaop, puertos from Computador where id=" + id,null);
                    if (fila.moveToFirst()){
                        lblNserie.setText("NSERIE: "+ fila.getString(0));
                        lblDescripcion.setText("DESCRIPCION: " + fila.getString(1));
                        lblprocesador.setText("PROCESADOR: " + fila.getString(2));
                        lblConectividad.setText("CONECTIVIDAD: " + fila.getInt(3));
                        lblMemoria.setText("MEMORIA: " + fila.getString(4));
                        lblDiscoduro.setText("DISCODURO: " + fila.getString(5));
                        lblSitemaop.setText("SISTEMAOP: " + fila.getString(6));
                        lblPuertos.setText("PUERTOS: " + fila.getString(7));
                        db.close();
                    }else {
                        Toast.makeText(getApplicationContext(), "No existe el computador", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    Log.e("error", e.getMessage());
                }
            }
        });
    }
}