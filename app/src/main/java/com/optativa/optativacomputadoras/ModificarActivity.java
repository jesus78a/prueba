package com.optativa.optativacomputadoras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.optativa.optativacomputadoras.BaseDeDatosSqlite.OpenHelper;

public class ModificarActivity extends AppCompatActivity {

    EditText txtModNserie, txtModDescripcion, txtModProcesador, txtModConectividad, txtModMemoria, txtModDiscoduro, txtModSistemaop, txtModPuertos;
    TextView lblModId;
    Button btnModificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar);

        lblModId = findViewById(R.id.lblModificarId);
        txtModNserie = findViewById(R.id.txtModNserie);
        txtModDescripcion = findViewById(R.id.txtModDescripcion);
        txtModProcesador = findViewById(R.id.txtModProcesador);
        txtModConectividad = findViewById(R.id.txtModConectividad);
        txtModMemoria = findViewById(R.id.txtModMemoria);
        txtModDiscoduro = findViewById(R.id.txtModDiscoduro);
        txtModSistemaop = findViewById(R.id.txtModSistemaop);
        txtModPuertos = findViewById(R.id.txtModPuertos);
        btnModificar = findViewById(R.id.btnModificarComputador);

        final int id = getIntent().getIntExtra("id",0);
        lblModId.setText(String.valueOf(id));
        txtModNserie.setText(getIntent().getStringExtra("nserie"));
        txtModDescripcion.setText(getIntent().getStringExtra("descripcion"));
        txtModProcesador.setText(getIntent().getStringExtra("procesador"));
        txtModConectividad.setText(String.valueOf(getIntent().getIntExtra("conectividad", 0)));
        txtModMemoria.setText(getIntent().getStringExtra("memoria"));
        txtModDiscoduro.setText(getIntent().getStringExtra("discoduro"));
        txtModSistemaop.setText(getIntent().getStringExtra("sistemaop"));
        txtModPuertos.setText(getIntent().getStringExtra("puertos"));

        btnModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { Modificar(id); }

            private void Modificar(int id) {
                OpenHelper conexion = new OpenHelper(getApplicationContext(), "Computador", null, 1);
                SQLiteDatabase db = conexion.getWritableDatabase();
                String nserie = txtModNserie.getText().toString();
                String descripcion = txtModDescripcion.getText().toString();
                String procesador = txtModProcesador.getText().toString();
                String conectividad = txtModConectividad.getText().toString();
                String memoria = txtModMemoria.getText().toString();
                String discoduro = txtModDiscoduro.getText().toString();
                String sistemaop = txtModSistemaop.getText().toString();
                String puertos = txtModPuertos.getText().toString();
                if (!nserie.isEmpty() && !descripcion.isEmpty()
                        && !procesador.isEmpty() && !conectividad.isEmpty() && !memoria.isEmpty()
                        && !discoduro.isEmpty() && !sistemaop.isEmpty() && !puertos.isEmpty()){
                    ContentValues modificar = new ContentValues();
                    modificar.put("nserie", nserie);
                    modificar.put("descripcion", descripcion);
                    modificar.put("procesador", procesador);
                    modificar.put("conectividad", conectividad);
                    modificar.put("memoria", memoria);
                    modificar.put("discoduro", discoduro);
                    modificar.put("sitemaop", sistemaop);
                    modificar.put("puertos", puertos);
                    db.update("Computador", modificar, "id="+id,null);
                    Toast.makeText(getApplicationContext(), "Se modificó correctamente", Toast.LENGTH_SHORT).show();
                    db.close();
                    startActivity(new Intent(getApplicationContext(), MostrarActivity.class));
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(), "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}